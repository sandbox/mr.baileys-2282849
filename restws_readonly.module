<?php

/**
 * Implements hook_rest_resource_info_alter().
 */
function restws_readonly_restws_resource_info_alter(&$resource_info) {
  foreach ($resource_info as &$resource) {
    $resource['base_class'] = $resource['class'];
    $resource['class'] = 'RestWSResourceControllerAccessDecorator';
  }
}

/**
 * Wrapper around a RestWS Resource Controller that overrides access control.
 *
 * For 'view' operations, access control is delegated to the original class. For
 * all other operations, access is denied, regardless of access control
 * implementation in the wrapped class.
 */
class RestWSResourceControllerAccessDecorator extends RestWSEntityResourceController {
  public function __construct($name, $info) {
    $info['class'] = $info['base_class'];
    unset($info['base_class']);
    return parent::__construct($name, $info);
  }

  /**
   * Overrides access(), denying access for all operations except 'view' and
   * 'query'.
   */
  public function access($op, $id) {
    if ($op === 'view' || $op === 'query') {
      return parent::access($op, $id);
    }
    else {
      drupal_add_http_header('Status', '405 Method not allowed');
      drupal_page_footer();
      exit;
    }
  }
}