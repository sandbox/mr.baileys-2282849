RESTful web services read-only.
===============================

Disables all non-read operations via RESTws. All non-read requests will return
an "403 Forbidden".

Note that the RestWS-module is secure by default in that it respects all
existing access checks on entities and resources, and protects against CSRF.
This module is only useful if you
a) want to prevent updates through REST for users that are otherwise authorized
to perform those updates (for example to prevent usage of authorized bots), or
b) are paranoid and prefer to have an additional layer of security.

Incompatibilities.
------------------
This module might be incompatible with any module for which all three of the
conditions below are true:
a) implements hook_restws_resource_info_alter(), AND
b) has a higher module weight than this module (i.e. runs after this module), AND
c) replaces the RestWS Resource Controller class that was defined through
hook_restws_resource_info().
